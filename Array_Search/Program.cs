﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraySearch
{
    class Program
    {
        static void Main(string[] args)
        // программа определяет позицию числа N в даном упорядоченном по возрастанию массиве целых чисел.
        {
            // вводим исходные данные.
            Console.Write("Введите массив: ");
            string S = Console.ReadLine() + " ";
            Console.Write("введите N: ");
            int N = Convert.ToInt32(Console.ReadLine());

            // обьявлем список, в котором будем хранить числа
            List<int> numbers = new List<int>();

            // Разбиваем введенную строку S на числа и заносим каждое число в отдельный элемент списка
            string D = "";
            for (int i = 0; i < S.Length; i++)
                if (S[i].Equals(' '))
                {
                    if (D.Length > 0)
                    {
                        numbers.Add(Convert.ToInt32(D));
                        D = "";
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    D = D + S[i];
                }

            // Определяем положение числа N в списке, если такого нет - возвращаем -1.


            int pos = -1;
            if (N >= numbers[0] && N <= numbers[numbers.Count - 1])
            {
                for (int i = 0; i < numbers.Count; i++)
                {
                    if (N == numbers[i])
                    {
                        pos = i;
                        break;
                    }

                }
            }
            // вывод
            Console.WriteLine();
            Console.Write("Результат: {0}", pos);

            Console.ReadKey();
        }
    }
}
